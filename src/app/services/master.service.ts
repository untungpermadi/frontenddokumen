import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { environment } from 'environments/environment';
import { ToastrService } from 'ngx-toastr';
import { Navigation, Router } from '@angular/router';
import { UserInterface } from './masterInterface';

@Injectable({
  providedIn: 'root'
})
export class MasterService {
  auth = new BehaviorSubject<Boolean>(false);
  USER: UserInterface;
  constructor(
    public httpClient: HttpClient,
    private toastService: ToastrService,
    public navController: Router,
  ) { }

  isAuthenticated(): boolean {
    if (!this.auth.value) {
      this.navController.navigateByUrl('login');
      return false;
    } else {
      return true;
    }
  }

  checkUser() {
    this.USER = JSON.parse(localStorage.getItem('user'));
    if (this.USER) {
      this.auth.next(true)
    }
  }

  presentToastSuccess(title, message) {
    this.toastService.success(message, title, {
      timeOut: 4000,
      closeButton: true,
      enableHtml: true,
      toastClass: "alert alert-success alert-with-icon",
      positionClass: "toast-top-right"
    })
  }

  presentToastErr(title, message) {
    this.toastService.error(message, title, {
      timeOut: 4000,
      closeButton: true,
      enableHtml: true,
      toastClass: "alert alert-danger alert-with-icon",
      positionClass: "toast-top-right"
    })
  }

  getDt(endpoint): Observable<any> {
    const httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
    }
    return this.httpClient.get(environment.api_url + endpoint, httpOption);
  }

  getNonJsonDt(endpoint): Observable<any> {
    const httpOption = {
      headers: new HttpHeaders({
        'Content-Type': '/*'
      }),
    }
    return this.httpClient.get(environment.api_url + endpoint, { responseType: 'blob' });
  }

  postDtJSON(endpoint, reqBody): Observable<any> {
    const httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
    }
    return this.httpClient.post(environment.api_url + endpoint, reqBody, httpOption);
  }

  deleteDt(endpoint, id): Observable<any> {
    const httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
    }
    return this.httpClient.delete(environment.api_url + endpoint + id, httpOption);
  }

  postDtFormDT(endpoint, reqBody): Observable<any> {
    return this.httpClient.post(environment.api_url + endpoint, reqBody);
  }

  putDtFormDT(endpoint, reqBody, id): Observable<any> {
    return this.httpClient.put(environment.api_url + endpoint + id, reqBody);
  }

  putDtJsonDT(endpoint, reqBody, id): Observable<any> {
    const httpOption = {
      headers: new HttpHeaders({
        'Content-Type': 'application/json'
      }),
    }
    return this.httpClient.put(environment.api_url + endpoint + id, reqBody, httpOption);
  }
}

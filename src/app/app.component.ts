import { Component } from '@angular/core';
import { MasterService } from './services/master.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})

export class AppComponent {

  constructor(
    public masterService: MasterService
  ) {
    this.masterService.checkUser()
  }
}

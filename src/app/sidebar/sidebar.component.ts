import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MasterService } from 'app/services/master.service';
import { UserInterface } from 'app/services/masterInterface';


export interface RouteInfo {
    path: string;
    title: string;
    icon: string;
    class: string;
}

export const ROUTES_ADMIN: RouteInfo[] = [
    // { path: '/dashboard', title: 'Dashboard', icon: 'nc-bank', class: '' },
    // { path: '/icons', title: 'Icons', icon: 'nc-diamond', class: '' },
    // { path: '/maps', title: 'Maps', icon: 'nc-pin-3', class: '' },
    // { path: '/notifications', title: 'Notifications', icon: 'nc-bell-55', class: '' },
    { path: '/nodin', title: 'Nota Dinas', icon: 'nc-tile-56', class: '' },
    { path: '/disposisi', title: 'Disposisi', icon: 'nc-tile-56', class: '' },
    // { path: '/table', title: 'Dokumen', icon: 'nc-tile-56', class: '' },
    { path: '/upload/new', title: 'Upload Dokumen', icon: 'nc-single-copy-04', class: '' },
    { path: '/listUser', title: 'Pengguna', icon: 'nc-badge', class: '' },
    // { path: '/user', title: 'BAWAAN USER', icon: 'nc-single-02', class: '' },
    { path: '/logout', title: 'Log-Out', icon: 'nc-user-run', class: '' },
    // { path: '/table', title: 'Table List', icon: 'nc-tile-56', class: '' },
    // { path: '/typography', title: 'Typography', icon: 'nc-caps-small', class: '' },
];

export const ROUTES: RouteInfo[] = [
    // { path: '/dashboard', title: 'Dashboard', icon: 'nc-bank', class: '' },
    // { path: '/icons', title: 'Icons', icon: 'nc-diamond', class: '' },
    // { path: '/maps', title: 'Maps', icon: 'nc-pin-3', class: '' },
    // { path: '/notifications', title: 'Notifications', icon: 'nc-bell-55', class: '' },
    { path: '/nodin', title: 'Nota Dinas', icon: 'nc-tile-56', class: '' },
    { path: '/disposisi', title: 'Disposisi', icon: 'nc-tile-56', class: '' },
    // { path: '/table', title: 'Dokumen', icon: 'nc-tile-56', class: '' },
    { path: '/upload/new', title: 'Upload Dokumen', icon: 'nc-single-copy-04', class: '' },
    // { path: '/user', title: 'BAWAAN USER', icon: 'nc-single-02', class: '' },
    { path: '/logout', title: 'Log-Out', icon: 'nc-user-run', class: '' },
    // { path: '/table', title: 'Table List', icon: 'nc-tile-56', class: '' },
    // { path: '/typography', title: 'Typography', icon: 'nc-caps-small', class: '' },
];
@Component({
    moduleId: module.id,
    selector: 'sidebar-cmp',
    templateUrl: 'sidebar.component.html',
})

export class SidebarComponent implements OnInit {
    public menuItems: any[];
    USER: UserInterface;
    constructor(
        private router: Router,
        private masterService: MasterService
    ) {

    }
    ngOnInit() {
        this.USER = JSON.parse(localStorage.getItem('user'))

        if (this.USER.userType == 2) {
            this.menuItems = ROUTES.filter(menuItem => menuItem);

        } else {
            this.menuItems = ROUTES_ADMIN.filter(menuItem => menuItem);
        }
    }

    onClick(url) {
        if (url != '/logout') {
            this.router.navigateByUrl(url)
        } else {
            this.logout()
            localStorage.removeItem('user')
        }
    }

    logout() {
        this.masterService.auth.next(false)
        this.router.navigateByUrl('')
        localStorage.removeItem('user')
    }
}

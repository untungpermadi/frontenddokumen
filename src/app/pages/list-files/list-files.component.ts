import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { MasterService } from 'app/services/master.service';
import { FilesInterface, TipeFilesInterface } from 'app/services/masterInterface';
import { environment } from 'environments/environment';

@Component({
  selector: 'app-list-files',
  templateUrl: './list-files.component.html',
  styleUrls: ['./list-files.component.css']
})
export class ListFilesComponent implements OnInit {

  LIST_FILES: FilesInterface[];
  TIPE_FILES: TipeFilesInterface[];
  constructor(
    public masterService: MasterService,
    public activatedRoute: ActivatedRoute,
    public router: Router
  ) { }

  goTo(url) {
    this.router.navigateByUrl(url)
  }

  ngOnInit(): void {
    this.getTipeFiles();
    this.getDisposisi();
  }

  getTipeFiles() {
    this.LIST_FILES = [];
    this.masterService.getDt(environment.endpoint_url.type_file).subscribe((dt: TipeFilesInterface[]) => {
      if (dt) {
        this.TIPE_FILES = dt;
      }
    })
  }

  getDisposisi() {
    this.LIST_FILES = [];
    this.masterService.getDt(environment.endpoint_url.list_file).subscribe((dt: FilesInterface[]) => {
      if (dt) {
        if (this.TIPE_FILES) {
          dt.forEach(element => {
            element.tipeFileString = this.TIPE_FILES.find(el => el.id == element.tipeFile) ? this.TIPE_FILES.find(el => el.id == element.tipeFile).fileTypes : element.tipeFile.toString();
          });
        }
        this.LIST_FILES = dt.filter(el => el.tipeFile == 2);
      }
    })
  }

}

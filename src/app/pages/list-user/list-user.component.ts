import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { MasterService } from 'app/services/master.service';
import { UserInterface } from 'app/services/masterInterface';
import { environment } from 'environments/environment';

@Component({
  selector: 'app-list-user',
  templateUrl: './list-user.component.html',
  styleUrls: ['./list-user.component.css']
})
export class ListUserComponent implements OnInit {
  LIST_USERS: UserInterface[];
  USER_TYPE: any[]
  constructor(
    public router: Router,
    public masterService: MasterService
  ) { }

  goTo(url) {
    this.router.navigateByUrl(url)
  }

  ngOnInit(): void {
    this.getUserTYpe()
    this.getListUser()
  }

  getUserTYpe() {
    this.USER_TYPE = [];
    this.masterService.getDt(environment.endpoint_url.user_type).subscribe((dt) => {
      if (dt) {
        this.USER_TYPE = dt;
      }
    })
  }

  getListUser() {
    this.LIST_USERS = [];
    this.masterService.getDt(environment.endpoint_url.list_user).subscribe((dt: UserInterface[]) => {
      if (dt) {
        if (this.USER_TYPE) {
          dt.forEach(element => {
            element.userType = this.USER_TYPE.find(el => el.id == element.userType).typeUser;
          });
        }
        this.LIST_USERS = dt;
      }
    })
  }

}

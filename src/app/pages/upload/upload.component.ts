import { AfterViewInit, Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MasterService } from 'app/services/master.service';
import { environment } from 'environments/environment';
import { FilesInterface, UserInterface } from 'app/services/masterInterface';
import { ToastrService } from 'ngx-toastr';
import { ActivatedRoute, NavigationEnd, Router } from '@angular/router';
@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.css']
})
export class UploadComponent implements OnInit, AfterViewInit, OnDestroy {
  uploadForm: FormGroup;
  TITLE_PAGE: string = 'Tambah Dokumen Baru';
  USER: UserInterface;
  constructor(
    public fb: FormBuilder,
    public masterService: MasterService,
    public toastService: ToastrService,
    public activatedRoute: ActivatedRoute,
    private router: Router
  ) {
    this.createUploadForm();
    if (this.activatedRoute.snapshot.paramMap.get('id') != 'new') {
      this.TITLE_PAGE = 'Detail Dokumen'
      this.getDetailFile(this.activatedRoute.snapshot.paramMap.get('id'))
    } else {
      this.ngOnDestroy()
    }
  }

  ngOnInit(): void {
    this.USER = JSON.parse(localStorage.getItem('user'));
  }

  ngOnDestroy() {
    this.uploadForm.reset()
  }

  ngAfterViewInit() {

  }

  patchFormValue(dt: FilesInterface) {
    this.uploadForm.get('file').patchValue(dt.file);
    this.uploadForm.get('fileName').patchValue(dt.fileName);
    this.uploadForm.get('idUser').patchValue(dt.idUser);
    this.uploadForm.get('tipeFile').patchValue(dt.tipeFile);
    this.downloadFile(dt.id)
  }

  createUploadForm() {
    this.uploadForm = this.fb.group({
      fileName: new FormControl(null, Validators.required),
      idUser: new FormControl(1),
      tipeFile: new FormControl(null, Validators.required),
      file: new FormControl(null, Validators.required)
    })
  }

  update() {
    const formData = new FormData();
    formData.append('file', this.uploadForm.get('file').value);
    formData.append('fileName', this.uploadForm.get('fileName').value);
    formData.append('idUser', this.uploadForm.get('idUser').value);
    formData.append('tipeFile', this.uploadForm.get('tipeFile').value);

    this.masterService.putDtFormDT(environment.endpoint_url.list_file, formData, this.activatedRoute.snapshot.paramMap.get('id')).subscribe((dt: FilesInterface) => {
      if (dt.id) {
        this.goBack(this.uploadForm.get('tipeFile').value)
        this.masterService.presentToastSuccess('Berhasil merubah data dokumen', 'SUKSES');
        this.ngOnDestroy()
      } else {
        this.masterService.presentToastErr('Gagal merubah data dokumen', 'ERROR');
      }
    }, err => {
      this.masterService.presentToastErr('Terdapat masalah, silahkan kontak administrator', 'GAGAL');
    })
  }

  delete() {
    this.masterService.deleteDt(environment.endpoint_url.list_file, this.activatedRoute.snapshot.paramMap.get('id')).subscribe(dt => {
      if (!dt) {
        this.goBack(this.uploadForm.get('tipeFile').value)
        this.masterService.presentToastSuccess('Berhasil menghapus data dokumen', 'SUKSES');
        this.ngOnDestroy()

      }
    })
  }

  submit() {
    const formData = new FormData();
    formData.append('file', this.uploadForm.get('file').value);
    formData.append('fileName', this.uploadForm.get('fileName').value);
    formData.append('idUser', this.USER.id.toString());//this.uploadForm.get('idUser').value);
    formData.append('tipeFile', this.uploadForm.get('tipeFile').value);

    this.masterService.postDtFormDT(environment.endpoint_url.list_file, formData).subscribe((dt: FilesInterface) => {
      if (dt.id) {
        this.goBack(this.uploadForm.get('tipeFile').value)
        this.masterService.presentToastSuccess('Dokumen ' + this.uploadForm.get('fileName').value + ' berhasil ditambahkan', 'SUKSES')
        this.ngOnDestroy()
      } else {
        this.masterService.presentToastErr('Terdapat kesalahan, harap perbaiki form', 'ERROR')
      }
    }, err => {
      this.masterService.presentToastErr('Terdapat masalah, silahkan kontak administrator', 'GAGAL')
    })
  }

  onChangeFile(event) {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.uploadForm.get('file').patchValue(file);
    }
  }

  getDetailFile(id) {
    this.masterService.getDt(environment.endpoint_url.list_file + id).subscribe((dt: FilesInterface) => {
      if (dt) {
        this.patchFormValue(dt)
      } else {
        this.masterService.presentToastErr('File tidak ditemukan', 'ERROR')
      }
    }, err => {
      this.masterService.presentToastErr('Terdapat masalah, silahkan kontak administrator', 'GAGAL')
    })
  }

  downloadFile(id) {
    this.masterService.getNonJsonDt(environment.endpoint_url.list_file + 'download/' + id).subscribe(dt => {
      if (dt) {
        let file = new File([dt], "document.pdf", { type: "application/pdf", lastModified: new Date().getTime() });
        let container = new DataTransfer();
        container.items.add(file);
        this.uploadForm.get('file').patchValue(container.files[0])
      }
    })
  }

  goBack(param) {
    switch (param) {
      case 1:
        this.router.navigateByUrl('nodin')
        break;
      case 2:
        this.router.navigateByUrl('disposisi')
        break;
    }
  }

}

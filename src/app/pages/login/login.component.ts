import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MasterService } from 'app/services/master.service';
import { environment } from 'environments/environment';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  constructor(
    private fg: FormBuilder,
    private masterService: MasterService
  ) { }

  ngOnInit(): void {
    this.createLoginForm()
  }

  createLoginForm() {
    this.loginForm = this.fg.group({
      nip: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required),
    })
  }

  login() {
    this.masterService.postDtJSON(environment.endpoint_url.list_user + 'login', this.loginForm.getRawValue()).subscribe((dt: any) => {
      if (dt.id) {
        localStorage.setItem('user', JSON.stringify(dt))
        this.masterService.auth.next(true)
        this.masterService.presentToastSuccess('Selamat Datang ' + dt.firstName + ' ' + dt.lastName, 'Halo..')
      } else {
        this.masterService.presentToastErr('Password salah', 'Login gagal')
      }
    }, err => {
      this.masterService.presentToastErr(err.error.error, 'Login gagal')
    })
  }

}


import { ComponentFixture, TestBed } from '@angular/core/testing';

import { NotaDinasComponent } from './nota-dinas.component';

describe('NotaDinasComponent', () => {
  let component: NotaDinasComponent;
  let fixture: ComponentFixture<NotaDinasComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ NotaDinasComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(NotaDinasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

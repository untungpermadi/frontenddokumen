import { Component, OnDestroy, OnInit } from '@angular/core';
import { FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { MasterService } from 'app/services/master.service';
import { UserInterface } from 'app/services/masterInterface';
import { environment } from 'environments/environment';

@Component({
  selector: 'app-user-profile',
  templateUrl: './user-profile.component.html',
  styleUrls: ['./user-profile.component.css']
})
export class UserProfileComponent implements OnInit, OnDestroy {
  ICON_LOGIN: string = 'nc-lock-circle-open';
  userForm: FormGroup;
  constructor(
    public masterService: MasterService,
    private fb: FormBuilder,
    private activatedRoute: ActivatedRoute,
    private router: Router
  ) {

  }

  ngOnInit(): void {
    this.createUserForm();
    if (this.activatedRoute.snapshot.paramMap.get('id') != 'new') {
      this.getDetailUser(this.activatedRoute.snapshot.paramMap.get('id'))
    }
  }

  ngOnDestroy() {
    this.userForm.reset()
  }

  createUserForm() {
    this.userForm = this.fb.group({
      firstName: new FormControl(null, Validators.required),
      lastName: new FormControl(null),
      userType: new FormControl(null, Validators.required),
      nip: new FormControl(null, Validators.required),
      password: new FormControl(null, Validators.required),
    })
  }

  showPassword() {
    if (this.ICON_LOGIN === 'nc-lock-circle-open') {
      this.ICON_LOGIN = 'nc-key-25'
      document.getElementById('pwd').setAttribute('type', 'string')
    } else {
      this.ICON_LOGIN = 'nc-lock-circle-open'
      document.getElementById('pwd').setAttribute('type', 'password')
    }

  }

  getDetailUser(id) {
    this.masterService.getDt(environment.endpoint_url.list_user + id).subscribe((dt: UserInterface) => {
      if (dt.id) {
        this.patchValueForm(dt)
      }
    })
  }

  postPengguna(reqBody) {
    this.masterService.postDtJSON(environment.endpoint_url.list_user, reqBody).subscribe((dt: UserInterface) => {
      if (dt.id) {
        this.masterService.presentToastSuccess('Berhasil menambahkan pengguna baru', 'SUKSES')
        this.router.navigateByUrl('listUser')
        this.ngOnDestroy()

      } else {
        this.masterService.presentToastErr('Cek kelengkapan Form', 'GAGAL')
      }
    }, err => {
      this.masterService.presentToastErr('Hubungi Administrator', 'ERROR')
    })
  }

  patchPengguna(reqBody, id) {
    this.masterService.putDtJsonDT(environment.endpoint_url.list_user, reqBody, id).subscribe((dt: UserInterface) => {
      if (dt.id) {
        this.masterService.presentToastSuccess('Berhasil mengubah data pengguna', 'SUKSES')
        this.router.navigateByUrl('listUser')
        this.ngOnDestroy()
      } else {
        this.masterService.presentToastErr('Cek kelengkapan Form', 'GAGAL')
      }
    }, err => {
      this.masterService.presentToastErr('Hubungi Administrator', 'ERROR')
    })
  }

  deletePengguna(id) {
    this.masterService.deleteDt(environment.endpoint_url.list_user, id).subscribe((dt) => {
      if (!dt) {
        this.masterService.presentToastSuccess('Berhasil menghapus data pengguna', 'SUKSES');
        this.router.navigateByUrl('listUser')
        this.ngOnDestroy()
      }
    })
  }

  patchValueForm(dt: UserInterface) {
    this.userForm.get('firstName').patchValue(dt.firstName);
    this.userForm.get('lastName').patchValue(dt.lastName);
    this.userForm.get('nip').patchValue(dt.nip);
    this.userForm.get('userType').patchValue(dt.userType);
    this.userForm.get('password').patchValue(dt.password);
  }

  submit() {
    this.postPengguna(this.userForm.getRawValue())
  }

  update() {
    this.patchPengguna(this.userForm.getRawValue(), this.activatedRoute.snapshot.paramMap.get('id'))
  }

  delete() {
    this.deletePengguna(this.activatedRoute.snapshot.paramMap.get('id'))
  }
}
import { Routes } from '@angular/router';

import { DashboardComponent } from '../../pages/dashboard/dashboard.component';
import { UserComponent } from '../../pages/user/user.component';
import { TableComponent } from '../../pages/table/table.component';
import { TypographyComponent } from '../../pages/typography/typography.component';
import { IconsComponent } from '../../pages/icons/icons.component';
import { MapsComponent } from '../../pages/maps/maps.component';
import { NotificationsComponent } from '../../pages/notifications/notifications.component';
import { UpgradeComponent } from '../../pages/upgrade/upgrade.component';
import { ListFilesComponent } from 'app/pages/list-files/list-files.component';
import { NotaDinasComponent } from 'app/pages/nota-dinas/nota-dinas.component';
import { UploadComponent } from 'app/pages/upload/upload.component';
import { ListUserComponent } from 'app/pages/list-user/list-user.component';
import { UserProfileComponent } from 'app/pages/user-profile/user-profile.component';
import { AuthGuard } from 'app/auth.guard';
import { LoginComponent } from 'app/pages/login/login.component';

export const AdminLayoutRoutes: Routes = [
    { path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard] },
    { path: 'user', component: UserComponent, canActivate: [AuthGuard] },
    { path: 'table', component: TableComponent, canActivate: [AuthGuard] },
    { path: 'typography', component: TypographyComponent, canActivate: [AuthGuard] },
    { path: 'icons', component: IconsComponent, canActivate: [AuthGuard] },
    { path: 'maps', component: MapsComponent, canActivate: [AuthGuard] },
    { path: 'notifications', component: NotificationsComponent, canActivate: [AuthGuard] },
    { path: 'upgrade', component: UpgradeComponent, canActivate: [AuthGuard] },
    { path: 'disposisi', component: ListFilesComponent, canActivate: [AuthGuard] },
    { path: 'nodin', component: NotaDinasComponent, canActivate: [AuthGuard] },
    { path: 'upload/:id', component: UploadComponent, canActivate: [AuthGuard] },
    { path: 'listUser', component: ListUserComponent, canActivate: [AuthGuard] },
    { path: 'profil/:id', component: UserProfileComponent, canActivate: [AuthGuard] },
    { path: 'login', component: LoginComponent }
];

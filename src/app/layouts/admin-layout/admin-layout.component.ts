import { Component, OnInit } from '@angular/core';
import { MasterService } from 'app/services/master.service';
import { UserInterface } from 'app/services/masterInterface';


@Component({
  selector: 'app-admin-layout',
  templateUrl: './admin-layout.component.html',
  styleUrls: ['./admin-layout.component.scss']
})
export class AdminLayoutComponent implements OnInit {
  constructor(
    public masterService: MasterService
  ) {

  }
  ngOnInit() {
    let usr: UserInterface = JSON.parse(localStorage.getItem('user'));
    if (usr) {
      this.masterService.auth.next(true)
    }
  }
}
